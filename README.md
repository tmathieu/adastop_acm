# Statistical comparison of stochastic algorithm with minimal computation requirements

Adaptive stopping for comparison of fully-specified stochastic algorithms.

## Reproducing AdaStop decision on the scores 

For reproduction, if `guix` is available on the system it will be used and no other installation is needed. If `guix` is installed, use 
```bash
source adastop_alias.sh
```
to define an alias for adastop that uses `guix` in the background to install/execute adastop.

On the other hand, if `guix` is not installed, one has to install `adastop` with `pip install adastop` prior to launching the script. The comparison can be re-run by executing `adastop_experiment.sh`, the results are then logged in the `results` directory.

## Recomputing the scores

The data (i.e. scores) for the experiment are in the `data` folder. The `data` folder also contains an `src` folders which contains all the information needed to recompute all the scores needed for our comparisons. We included tutorials which use `guix` to reproduce the global optimization and the energy consumption experiments from the paper. 
