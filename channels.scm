(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "18393fcdddf5c3d834fa89ebf5f3925fc5b166ed")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'adastop)
        (url "https://github.com/TimotheeMathieu/adastop")
        (branch "guix")
        (commit
          "52c5fa8827a468aa46d3dd24745a362079502ac7"))
     )
