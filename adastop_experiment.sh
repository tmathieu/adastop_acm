#!/usr/bin/env -S bash

[ ! -z "$(ls -A results)" ] && rm results/* # If result directory not empty, empty it

mkdir results

# Define the adastop commands.
PATH_CHANNEL=$(realpath channels.scm)

adastop () {
    guix time-machine --channels=$PATH_CHANNEL -- shell -CN python-adastop -- adastop $@
    }

do_reset () {
  adastop reset $1
}
do_plot () {
  adastop plot $1 $2 --width 10
}


# Run the comparisons

counter=42

for experiment in benchopt_ML global_opti languages languages_guix; do
    echo processing experiment $experiment

    if [ "$experiment" = "benchopt_ML" ]; then
        N=6
        K=5
    fi
    if [ "$experiment" = "global_opti" ]; then
        N=10
        K=10
    fi
    if [ "$experiment" = "languages" ]; then
        N=5
        K=10
    fi
    if [ "$experiment" = "languages_guix" ]; then
        N=5
        K=10
    fi
    

    # Remove litter files from adastop if they exist
    [ -f data/$experiment/.adastop_comparator.pkl ] &&  do_reset data/$experiment

    # Do the comparisons until adastop say it is finished
    readarray -d '' entries < <(printf '%s\0' data/$experiment/*.csv | sort -zV)

    for file in "${entries[@]}"; do
        echo processing $file
        adastop compare --alpha 0.01 --seed $counter --size-group $N --n-groups $K $file  >> results/${experiment}.txt
        counter=$((counter+1))
        isInFile=$(cat results/${experiment}.txt | grep -c "finished")
        if [ $isInFile -eq 1 ]; then
            do_plot data/$experiment results/${experiment}.pdf
            break
        fi
    done
    echo " "
    echo "results can be found in results/${experiment}.txt"
    echo " "
done
