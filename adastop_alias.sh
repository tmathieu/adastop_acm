PATH_CHANNEL=$(realpath channels.scm)

adastop () {
    guix time-machine --channels=$PATH_CHANNEL -- shell -CN python-adastop -- adastop $@
    }
