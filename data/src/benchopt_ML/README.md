# Machine Learning benchmark

This folder contains the code necessary to generate the csv files in `../../data/benchopt_ML`.

## Dependencies and installation

In order to generate the files, you have to install the [benchopt python library](https://benchopt.github.io/) and clone [the PAPER_hyperparam_sens brahcn of the benchmark repository](https://github.com/benchopt/benchmark_resnet_classif/tree/PAPER_hyperparam_sens). benchopt library has to be version 1.3.1, all other dependencies are handled through benchopt using the yml file `benchopt_config.yml`.

```
git clone https://github.com/benchopt/benchmark_resnet_classif
cd benchmark_resnet_classif 
conda create -n benchopt python
conda activate benchopt 
pip install benchopt==1.3.1
benchopt install .
```

## Usage

In order to generate the file `../../data/benchopt_ML/adastop_resnet_0.csv` of scores with the algorithms all the algorithms, use

```
benchopt run . --config benchopt_config.yml --env -r 6 --output adastop_resnet_0
```

This will generate a parquet file in the `outputs` directory that can be converted to csv through the `process_benchopt.py` script.

If you want to compute for a reduced number of FSED, please edit the yml as instructed in benchopt user guide.
