#!/usr/bin/env -S guix time-machine --channels=../../../channels.scm -- shell -CF python python-scikit-opt python-cma python-scipy python-pandas python-tqdm -- python3

import numpy as np
from tqdm import tqdm
import pandas as pd
import os

from scipy.optimize import differential_evolution
import cma
from sko.PSO import PSO
from functools import partial
import sys
import argparse

n_cpu = 8

os.environ["OMP_NUM_THREADS"] =str(n_cpu) # export OMP_NUM_THREADS=1
os.environ["OPENBLAS_NUM_THREADS"] = str(n_cpu) # export OPENBLAS_NUM_THREADS=1
os.environ["MKL_NUM_THREADS"] = str(n_cpu) # export MKL_NUM_THREADS=1
os.environ["VECLIB_MAXIMUM_THREADS"] = str(n_cpu) # export VECLIB_MAXIMUM_THREADS=1
os.environ["NUMEXPR_NUM_THREADS"] = str(n_cpu) # export NUMEXPR_NUM_THREADS=1

# parameters
parser = argparse.ArgumentParser()
parser.add_argument("--id-batch", help = "Identifier for this batch", type = int)
parser.add_argument("--seed", help = "Initial seed for reproducibility", type = int)
parser.add_argument("-N", help="Size of a group", type=int)
parser.add_argument("--fsed", help="Names of FSED to compute, in {cma, de, pso}", type=str)

args = parser.parse_args()

rng_init = np.random.RandomState(args.seed)
seed = rng_init.randint(2**30, size=10)[args.id_batch]
N = args.N
d = 10
D = 100 # Bound on domain
max_iter = 100000
true_minimum = -450
print(args.fsed)
rng = np.random.RandomState(42) # fix the seed for function generation
optimum = rng.uniform(size=d)*D

def obj(x, seed):
    rng = np.random.RandomState(seed)
    z = x - optimum
    return np.sum([np.sum(z[:i])**2 for i in range(d)])*(1+0.4*np.abs(rng.normal()))+true_minimum

def solve_de(seed):
    result = differential_evolution(partial(obj,seed=seed), [(-D,D)]*d, tol=1e-8, polish=False,maxiter=max_iter, seed=seed, disp=True)
    return result.fun

def solve_cma(seed):
    opts = cma.CMAOptions()
    opts.set('tolfun', 1e-8)
    opts.set("maxfevals", max_iter)
    res = cma.fmin(lambda x : obj(x, seed), d * [1], D/3)
    return obj(res[5], seed)

def solve_pso(seed):
    pso = PSO(func=lambda x : obj(x, seed), n_dim=d, pop=100, max_iter=max_iter, 
              lb=[-D]*d, ub=[D]*d, w=0.5, c1=2.8, c2=1.3)
    pso.run(precision=1e-8)
    return pso.gbest_y

# compute the score
dic = {}
if "cma" in args.fsed:
    dic["CMA"] = [np.abs(solve_cma(seed+f)-true_minimum) for f in range(N)]
if "de" in args.fsed:
    dic["DE"] = [np.abs(solve_de(seed+f) -true_minimum) for f in range(N)]
if "pso" in args.fsed:
    dic["pso"] =  [np.abs(solve_pso(seed+f)-true_minimum)[0] for f in range(N)]
    
# export to csv file
pd.DataFrame(dic).to_csv("swarm"+str(args.id_batch)+".csv")
