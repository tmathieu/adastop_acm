# Tutorial about the use of adastop

This tutorial explains how to reproduce the experiments of section 3.2 related to the comparison of global optimisation algorithms.

## Preliminaries

We assume you use a standard Linux distribution, such as Ubuntu, with usual development tools installed.

You need to install `guix`:

```bash
sudo apt-get install guix
```

## Global minimization study

Let us replicate the results from section 3.2, the comparison of global optimisation algorithms.

Let us suppose your current directory is `adastop_acm/data/src/global_opti`.

Now, we are following the adastop methodology: we decide on K, N, and alpha and we first run N times the FSEDs to compare. In this case study, there are 3 FSED, one per optimisation algorithm.
In the paper, we take K=10, N=10, and alpha = 0.01.

We provide a script that uses guix container to generate scores. It is equivalent to running N times each FSED, and stored the scores in a csv file.

To make the first run:

```bash
./swarm.py --id-batch 1 --seed 123 -N 10 --fsed "cma de pso"
```

The first argument `--id-batch` is the number of the run (first => 1) and the second argument is the value of N. The second is the initial seed of the random number generator, note that `123` is arbitrary. If you want to obtain the same results as in this tutorial, you have to use this value. Otherwise, feel free to use whichever seed value you like.

While running, this script prints out messages.

The script `swarp.py` creates one file containing the N scores for the 3 FSEDs located in `swarm1.csv`. This file is very simple: there are 4 columns: the first is just the number of the run, and then, columns 2 to 4 contains the N scores for each of the 3 FSEDs (CMA-ES, DE, PSO).

Now, we perform the adastop test on this set of scores to see whether we have to go on, on whether adastop can readily decide. However, as above, for the first run of adastop on a new experiment, we have to initialize it.

```bash
adastop reset .
```

Then, we perform the test on the first set of N scores:
```bash
adastop compare --size-group 10 --n-groups 10 --alpha 0.01 --seed 121344321 swarm1.csv
```

As above, in this command, the arguments `--n-groups 10 --alpha 0.01` specify the value of K and alpha.

adastop prints out `Still undecided about CMA DE pso` which means that we have to perform an other round of collection of scores and adastop test. And so on and so forth as long as either adastop prints out `Test is finished, decisions are` followed by a table with the test results.

Then, we gather a new batch of scores. 

```bash
./swarm.py --id-batch 2 --seed 123 -N 10 --fsed "cma de pso"
```
and we compare,
```bash
adastop compare --size-group 10 --n-groups 10 --alpha 0.01 --seed 121344322 swarm2.csv
```
We continue until there is nothing to compare anymore.
```bash
./swarm.py --id-batch 3 --seed 123 -N 10 --fsed "cma de pso"
```
```bash
adastop compare --size-group 10 --n-groups 10 --alpha 0.01 --seed 121344323 swarm3.csv
```
```bash
./swarm.py --id-batch 4 --seed 123 -N 10 --fsed "cma de pso"
```
```bash
adastop compare --size-group 10 --n-groups 10 --alpha 0.01 --seed 121344324 swarm4.csv
```
We get the following result:
```
Test is finished, decisions are
|    | Agent1 vs Agent2   |   mean Agent1 |   mean Agent2 |     mean diff |   std Agent 1 |   std Agent 2 | decisions   |
|---:|:-------------------|--------------:|--------------:|--------------:|--------------:|--------------:|:------------|
|  0 | CMA vs DE          |   2.84217e-14 |   1.37482e-06 |  -1.37482e-06 |   2.84217e-14 |   5.37008e-07 | smaller     |
|  0 | CMA vs PSO         |   2.84217e-14 |  65.2338      | -65.2338      |   2.84217e-14 | 140.233       | smaller     |
|  0 | DE vs PSO          |   1.37482e-06 |  65.2338      | -65.2338      |   5.37008e-07 | 140.233       | smaller     |

Comparator Saved
```

where we read that adastop is able to conclude that CMA obtain a smaller result than DE which is itself smaller than PSO.

We may get a graphical representation of this conclusion by running:

```bash
adastop plot . optimisation_results.png
```

![](optimisation_results.png)

