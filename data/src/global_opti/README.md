# Global optimization benchmark

This folder contains the code necessary to generate the csv files in `../../global_opti`.

## Dependencies

The script are written to be used with guix (installable via guix installer or using `sudo apt-get install guix` on a debian system), if guix is installed on your system there is nothing to do. On the other hand, by changing the shebang and installing the python libraries cma, scikit-opt, scipy, numpy, tqdm and pandas the script can be run without guix. 

## Usage

In order to generate the file `../../global_opti/swarm1.csv` of 10 scores with the algorithms CMA-ES, DE and PSO, use 
```
./swarm.py --id-batch 1 --seed 42 -N 10 --fsed "cma de pso"
mv swarm1.csv ../../global_opti/swarm1.csv
```



