# Tutorial about the use of adastop

This tutorial explains how to reproduce the experiments of section 3.1 related to the energy consumption of different languages.

## Preliminaries

We assume you use a standard Linux distribution, such as Ubuntu, with usual development tools installed.

You need to install `guix`:

```bash
sudo apt-get install guix
```

and `perf`:

```bash
sudo apt-get install linux-tools-generic linux-cloud-tools-generic
```

And you need to clone the repo:

```bash
git clone git@gitlab.inria.fr:tmathieu/adastop_acm.git
```

## Evaluating Guix energy consumption overhead

Section 3.1 contains a preliminary use of adastop to check whether Guix adds some energy consumption overhead. Let us replicate it.

First, let us `cd`:

```bash
cd adastop_acm/data/src/language_energy
```

Now, we are following the adastop methodology: we decide on K, N, and alpha and we first run N times the two FSEDs to compare, C and C-guix.
In the paper, we take K=10, N=6, and alpha = 0.01.

We provide a convenient bash script to generate the scores. This script is just meant to put the files in a certain directory, there is nothing fancy in it. We could have run N times each FSED, and stored the scores in a csv file.

To make the first run:

```bash
sudo ./make_data_guix.sh 1 6
```

The first argument of `make_data_guix.sh` is the number of the run (first => 1) and the second argument is the value of N.

While running, this script prints out messages.

`sudo` is needed in order to be able to access the energy consumption of the process and we preserve home in order to use user guix profile.

This bash script creates one file containing the N scores for both FSEDs located in `../../languages_guix/energy_guix_1.csv`. This file is very simple: there are 3 columns: the first is just the number of the run, the second column contains the N scores of the first FSED (stand-alone C), the third column contains the N scores of the secod FSED (C_guix).

Now, we perform the adastop test on this set of scores to see whether we have to go on, on whether adastop can readily decide. However, for the first run of adastop on a new experiment, we have to initialize it.

```bash
adastop reset ../../languages_guix
```

Then, we perform the test on the first set of N scores:
```bash
adastop compare --n-groups 10 --alpha 0.01 --seed 87617873 ../../languages_guix/energy_guix_1.csv
```

In this command, the arguments `--n-groups 10 --alpha 0.01` specify the value of K and alpha.

adastop prints out `Still undecided about c c-guix` which means that we have to perform an other round of collection of scores and adastop test. And so on and so forth as long as either adastop prints out `Test is finished, decisions are` followed by a table with the test results.

Please note that the value `87617873` is arbitrary: this is the seed for the pseudo-random number generator used in adastop. If you want to obtain the same results as in this tutorial, you have to use this value. Otherwise, feel free to use whichever seed value you like.

Here we go: we run `make_data_guix.sh` for a second round of collection of scores for both FSEDs:

```bash
sudo ./make_data_guix.sh 2 6
```

and run adastop on the whole set of collected scores:
```bash
adastop compare --n-groups 10 --alpha 0.01 --seed 7863817 ../../languages_guix/energy_guix_2.csv
```

and so on and so forth:
```bash
sudo ./make_data_guix.sh 3 6
adastop compare --n-groups 10 --alpha 0.01 --seed 1243 ../../languages_guix/energy_guix_3.csv
sudo ./make_data_guix.sh 4 6
adastop compare --n-groups 10 --alpha 0.01 --seed 1272143 ../../languages_guix/energy_guix_4.csv
sudo ./make_data_guix.sh 5 6
adastop compare --n-groups 10 --alpha 0.01 --seed 4872121 ../../languages_guix/energy_guix_5.csv
sudo ./make_data_guix.sh 6 6
adastop compare --n-groups 10 --alpha 0.01 --seed 314159 ../../languages_guix/energy_guix_6.csv
sudo ./make_data_guix.sh 7 6
adastop compare --n-groups 10 --alpha 0.01 --seed 783 ../../languages_guix/energy_guix_7.csv
sudo ./make_data_guix.sh 8 6
adastop compare --n-groups 10 --alpha 0.01 --seed 371 ../../languages_guix/energy_guix_8.csv
sudo ./make_data_guix.sh 9 6
adastop compare --n-groups 10 --alpha 0.01 --seed 783621 ../../languages_guix/energy_guix_9.csv
sudo ./make_data_guix.sh 10 6
adastop compare --n-groups 10 --alpha 0.01 --seed 7831 ../../languages_guix/energy_guix_10.csv
```

After these 10 rounds (K = 10, the whole budget is exhausted), adastop prints out:

```console
Test is finished, decisions are
|    | Agent1 vs Agent2   |   mean Agent1 |   mean Agent2 |   mean diff |   std Agent 1 |   std Agent 2 | decisions   |
|---:|:-------------------|--------------:|--------------:|------------:|--------------:|--------------:|:------------|
|  0 | c vs c-guix        |       37.5028 |       38.3578 |      -0.855 |       1.97023 |       2.50721 | equal       |

Comparator Saved
```

where we read that adastop is not able to find any statistically significant difference in the energy consumption made by the stand-alone C code, and the Guix embedded same C code.

We may get a graphical representation of this conclusion by running:

```bash
adastop plot ../../languages_guix/ ../../languages_guix/C.vs.C_guix.png
eog ../../languages_guix/C.vs.C_guix.png
```

![](./C.vs.C_guix.png)

There are two parts in this picture. The table at the top has 2 rows and 2 columns because we compare 2 FSEDs. Each row and each column corresponds to one FSED. At the intersection of a row and a column, we read the result of the comparison of the 2 FSEDs. In the case of this experiment, the cell indicates that the FSED in the row (the rightward arrow) is equal (the = symbol) to the FSED in the column (the downward arrow). To be precise, equal means ``adastop is not able to distinguish'', rather than a real equality. The 60's in the top row are the number of scores collected for each FSED to reach this conclusion.

The part below illustrates the distribution of scores for the FSEDs. For each FSED (column), a boxplot provides the following information: the median in orange, the average is represented with a green triangle, the top and bottom of the box are the first and third quartiles, the whiskers are the so-called hinges of the boxplot that extend from the box to the farthest data point lying within 1.5x the inter-quartile range (IQR) from the box, and the little circles indicate outliers.


It is very important to note that the results of this experiment are quite variable. They may strongly vary depending on the machine you run the experiment and its workload. Even on a laptop that just performs the test, the results may vary substantially. However, the qualitative result is always the same: there is no statistically significant difference in energy consumption whether you run the C code in a Guix container, or just as any other Linux command from a terminal.


## Comparison of programming languages

Now, let us move on to the core of section 3.1 case study, the comparison of the energy consumption of the same program written in different languages.

Let us suppose your current directory is `adastop_acm/data/src/language_energy`.

Now, exactly as above, we are following the adastop methodology: we decide on K, N, and alpha and we first run N times the FSEDs to compare. In this case study, there are 7 FSED, one per programming language.
In the paper, we take K=10, N=6, and alpha = 0.01.

As above, we provide a convenient bash script to generate the scores. This script is just meant to put the files in a certain directory, there is nothing fancy in it. We could have run N times each FSED, and stored the scores in a csv file.

To make the first run:

```bash
sudo ./make_data.sh 1 6 c cpp fortran pascal lisp julia pypy
```

As above, the first argument of `make_data.sh` is the number of the run (first => 1) and the second argument is the value of N.

While running, this script prints out messages.

As above, `sudo` is needed in order to be able to access the energy consumption of the process and we preserve home in order to use user guix profile.

This bash script creates one file containing the N scores for the 7 FSEDs located in `../../languages/energy_1.csv`. This file is very simple: there are 8 columns: the first is just the number of the run, and then, columns 2 to 8 contains the N scores for each of the 7 FSEDs (C, C++, Fortran, Pascal, Lisp, Julia, pypy).

Now, we perform the adastop test on this set of scores to see whether we have to go on, on whether adastop can readily decide. However, as above, for the first run of adastop on a new experiment, we have to initialize it.

```bash
adastop reset ../../languages
```

Then, we perform the test on the first set of N scores:
```bash
adastop compare --n-groups 10 --alpha 0.01 --seed 87617873 ../../languages/energy_1.csv
```

As above, in this command, the arguments `--n-groups 10 --alpha 0.01` specify the value of K and alpha.

adastop prints out `Still undecided about c-guix cpp fortran julia lisp pascal pypy` which means that we have to perform an other round of collection of scores and adastop test. And so on and so forth as long as either adastop prints out `Test is finished, decisions are` followed by a table with the test results.

Please note that the value `87617873` is arbitrary: this is the seed for the pseudo-random number generator used in adastop. If you want to obtain the same results as in this tutorial, you have to use this value. Otherwise, feel free to use whichever seed value you like.

Here we go: we run `make_data.sh` for a second round of collection of scores:

```bash
sudo ./make_data.sh 2 6 c cpp fortran pascal lisp julia pypy
```

and run adastop on the whole set of collected scores:
```bash
adastop compare --n-groups 10 --alpha 0.01 --seed 7863817 ../../languages/energy_2.csv
```

and so on and so forth:
```bash
sudo ./make_data.sh 3 6 c cpp fortran pascal lisp julia pypy
adastop compare --n-groups 10 --alpha 0.01 --seed 1243 ../../languages/energy_3.csv
sudo ./make_data.sh 4 6 c cpp fortran pascal lisp julia pypy
adastop compare --n-groups 10 --alpha 0.01 --seed 1272143 ../../languages/energy_4.csv
sudo ./make_data.sh 5 6 c cpp fortran pascal lisp julia pypy
adastop compare --n-groups 10 --alpha 0.01 --seed 4872121 ../../languages/energy_5.csv
```

```console
Test is finished, decisions are
|    | Agent1 vs Agent2   |   mean Agent1 |   mean Agent2 |   mean diff |   std Agent 1 |   std Agent 2 | decisions   |
|---:|:-------------------|--------------:|--------------:|------------:|--------------:|--------------:|:------------|
|  0 | fortran vs cpp     |       58.1063 |       35.3056 |    22.8008  |       1.66714 |       1.27956 | larger      |
|  0 | fortran vs pascal  |       58.1063 |       52.7925 |     5.31383 |       1.66714 |       1.59108 | larger      |
|  0 | fortran vs julia   |       58.1063 |       65.1908 |    -7.0845  |       1.66714 |       1.23241 | smaller     |
|  0 | fortran vs c-guix  |       58.1063 |       36.8517 |    21.2547  |       1.66714 |       2.42437 | larger      |
|  0 | fortran vs pypy    |       58.1063 |      384.289  |  -326.183   |       1.66714 |      17.2552  | smaller     |
|  0 | fortran vs lisp    |       58.1063 |       57.2323 |     0.874   |       1.66714 |       1.51794 | larger      |
|  0 | cpp vs pascal      |       35.3056 |       52.7925 |   -17.4869  |       1.27956 |       1.59108 | smaller     |
|  0 | cpp vs julia       |       35.3056 |       65.1908 |   -29.8853  |       1.27956 |       1.23241 | smaller     |
|  0 | cpp vs c-guix      |       35.3056 |       36.8517 |    -1.54611 |       1.27956 |       2.42437 | smaller     |
|  0 | cpp vs pypy        |       35.3056 |      384.289  |  -348.984   |       1.27956 |      17.2552  | smaller     |
|  0 | cpp vs lisp        |       35.3056 |       57.2323 |   -21.9268  |       1.27956 |       1.51794 | smaller     |
|  0 | pascal vs julia    |       52.7925 |       65.1908 |   -12.3983  |       1.59108 |       1.23241 | smaller     |
|  0 | pascal vs c-guix   |       52.7925 |       36.8517 |    15.9408  |       1.59108 |       2.42437 | larger      |
|  0 | pascal vs pypy     |       52.7925 |      384.289  |  -331.497   |       1.59108 |      17.2552  | smaller     |
|  0 | pascal vs lisp     |       52.7925 |       57.2323 |    -4.43983 |       1.59108 |       1.51794 | smaller     |
|  0 | julia vs c-guix    |       65.1908 |       36.8517 |    28.3392  |       1.23241 |       2.42437 | larger      |
|  0 | julia vs pypy      |       65.1908 |      384.289  |  -319.098   |       1.23241 |      17.2552  | smaller     |
|  0 | julia vs lisp      |       65.1908 |       57.2323 |     7.9585  |       1.23241 |       1.51794 | larger      |
|  0 | c-guix vs pypy     |       36.8517 |      384.289  |  -347.438   |       2.42437 |      17.2552  | smaller     |
|  0 | c-guix vs lisp     |       36.8517 |       57.2323 |   -20.3807  |       2.42437 |       1.51794 | smaller     |
|  0 | pypy vs lisp       |      384.289  |       57.2323 |   327.057   |      17.2552  |       1.51794 | larger      |

Comparator Saved
```

We may get a graphical representation of this conclusion by running:

```bash
adastop plot ../../languages_guix/ ../../languages_guix/C.vs.C_guix.png
eog ../../languages_guix/languages.png
```

![](./languages.png)

Detailed explanations about the content of this table may be found above. 
This table tells us that adastop concludes: C++ < C < pascal < lisp < fortran < julia < python/pypy, which is comparable but different from what is in the paper, this is due to the execution of this tutorial on a laptop different from the one used to generate the data for the article.
In the upper row, we also read the number of scores needed to get a particular conclusion. For instance, 12 scores were enough to conclude that C++ < Pascal, Julia, and Python/pypy.
Likewise, 18 scores were enough to tell that C < all others, except C++.

It is very important to note that the results of this experiment are quite variable. They may strongly vary depending on the machine you run the experiment and its workload. Even on a laptop that just performs the test, the results may vary substantially. However, the qualitative result does not vary a lot. We always get a ranking with C and C++ being the less energy consuming, sometimes equally, sometimes not; then, we very often have this group with Pascal, Fortran, Lisp, and Julia; and then, clearly the most energy consuming is Python. Indeed having Fortran in this middle group is surprising. One hypothesis is that the Fortran compiler that is used is not as efficient as it could be, but this is just a guess prompted by this suprising result.
