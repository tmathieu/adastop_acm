
bench_c ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell gcc python -- gcc -pipe -Wall -O3 -fomit-frame-pointer -march=native -mfpmath=sse -msse3 C/nbody.gcc-4.c -o /tmp/nbody.gcc-4.gcc_run -lm

    # Run
    touch /tmp/energy_c_$1.txt
    for f in $(seq 6); do
        result=`python3  utils/get_joules.py /tmp/nbody.gcc-4.gcc_run 50000000`
        sleep 3
        echo $result >> /tmp/energy_c_$1.txt
    done
    
}


bench_c_guix ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell gcc python -- gcc -pipe -Wall -O3 -fomit-frame-pointer -march=native -mfpmath=sse -msse3 C/nbody.gcc-4.c -o /tmp/nbody.gcc-4.gcc_run -lm

    # Run
    touch /tmp/energy_c-guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell  gcc python -- python3 utils/get_joules.py /tmp/nbody.gcc-4.gcc_run 50000000`
        sleep 3
        echo $result >> /tmp/energy_c-guix_$1.txt
    done
    
}



