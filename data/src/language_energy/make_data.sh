#!/usr/bin/env bash
if  test -n "$(find /tmp -maxdepth 1 -name 'energy*' -print -quit)"
  then rm /tmp/energy*.txt 
fi


for f in C C++ Julia Lisp Python Pascal  Fortran ; do 
    source $f/definitions.sh
done

for f in ${@:2}
do 
    echo "generating " $f
    bench_${f}_guix $1
    sleep 3
done

./utils/process_data.py $1
mv energy_$1.csv ../../languages
