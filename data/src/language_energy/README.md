# Energy consumption of programming language study

This folder contains the code necessary to generate the csv files in `../../data/languages`.

## Dependencies

To run the code of this folder, it is necessary to have guix installed (either through package manager like, e.g.  `sudo apt install guix`, or using [guix installer](https://guix.gnu.org/manual/en/html_node/Binary-Installation.html)), generic core utils from Linux (cURL, bash...), as well as the perf command line linux utility. Additionally, for "Guix energy consumption" it is necessary to have python and gcc installed.
Note that it is not necessary to install any of the programming language, this will be handled through guix.

*Remark:* `perf` has to be installed and activated in kernel settings. On Ubuntu this can be done by installing `linux-tools-generic` and `linux-cloud-tools-generic` (or the package corresponding to your kernel if your kernel is not the latest). On Arch-Linux you can just install the `perf` package.

## Generating the csv files

### Energy consumption of programming language study

The csv files for energy consumption benchmark are saved under  `../../data/languages/energy_[1-10].csv`. For example, to generate `../../data/languages/energy_1.csv`, you can use the command 

```bash
sudo ./make_data.sh 1 c cpp fortran pascal lisp julia pypy
```
sudo is needed in order to be able to access the energy consumption of the process and we preserve home in order to use user guix profile (useful if perf have been installed through guix for instance). The integer argument to `make_data.sh` is used only for naming purpose, the other arguments are the languages with which we run the benchmark. 


### Guix energy consumption 

The csv files for guix energy consumption are saved under  `../../data/languages/energy_guix_[1-10].csv`. For example, to generate `../../data/languages/energy_guix_1.csv`, you can use the command 

```bash
sudo ./make_data_guix.sh 1
```

