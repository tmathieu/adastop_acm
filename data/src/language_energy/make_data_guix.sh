#!/usr/bin/env bash

if  test -n "$(find /tmp -maxdepth 1 -name 'energy*' -print -quit)"
  then rm /tmp/energy*.txt 
fi

source C/definitions.sh

bench_c_guix $1
sleep 2
bench_c $1

./utils/process_data.py $1
mv energy_$1.csv ../../languages_guix/energy_guix_$1.csv
