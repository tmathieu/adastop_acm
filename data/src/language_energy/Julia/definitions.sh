bench_julia_guix ()
{
    # Run
    touch /tmp/energy_julia_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell julia python -- python3  utils/get_joules.py julia -O3  -- Julia/nbody.julia-3.julia 50000000`
        sleep 3
        echo $result >> /tmp/energy_julia_guix_$1.txt
    done
    
}
