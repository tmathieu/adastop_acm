
bench_pascal_guix ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell fpc python -- fpc -FuInclude/fpascal -XXs -O4 -Tlinux -CfSSE3 -oFPASCAL_RUN Pascal/nbody.fpascal
    mv Pascal/FPASCAL_RUN /tmp/nbody.fpascal.run

    # Run
    touch /tmp/energy_pascal_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell fpc python -- python3  utils/get_joules.py /tmp/nbody.fpascal.run 50000000`
        sleep 3
        echo $result >> /tmp/energy_pascal_guix_$1.txt
    done
    
}
