bench_cpp_guix ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell  gcc python -- g++ -c -pipe -O3 -fomit-frame-pointer -march=native -mfpmath=sse -msse3 --std=c++11 C++/nbody.gpp-8.c++ -fPIE -o /tmp/nbody.gpp-8.c++.o &&  g++ /tmp/nbody.gpp-8.c++.o -o /tmp/nbody.gpp-8.gpp_run -fopenmp

    # Run
    touch /tmp/energy_cpp_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell  gcc python -- python3  utils/get_joules.py /tmp/nbody.gpp-8.gpp_run 50000000`
        sleep 3

        echo $result >> /tmp/energy_cpp_guix_$1.txt
    done
    
}
