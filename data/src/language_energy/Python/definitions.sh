bench_python_guix ()
{
    # Run
    touch /tmp/energy_python_guix_$1.txt
    for f in $(seq 6); do
        vanilla=`guix time-machine --channels=../../../channels.scm -- shell python -- python3  utils/get_joules.py python3 -OO Python/nbody.py 50000000`
        sleep 3
        echo $result >> /tmp/energy_python_guix_$1.txt
    done
    
}


bench_pypy_guix ()
{
    # Run
    touch /tmp/energy_pypy_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell python pypy -- python3  utils/get_joules.py pypy3 -OO Python/nbody.py 50000000`
        sleep 3
        echo $result >> /tmp/energy_pypy_guix_$1.txt
    done
    
}

