bench_lisp_guix ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell sbcl gcc python -- sbcl --load Lisp/nbody.lisp --eval "(save-lisp-and-die \"nbody.core\" :purify t :toplevel (lambda () (main) (quit)))"

    # Run
    touch /tmp/energy_lisp_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell gcc python sbcl -- python3  utils/get_joules.py sbcl --noinform --core nbody.core 50000000`
        sleep 3
        echo $result >> /tmp/energy_lisp_guix_$1.txt
    done
    
}

