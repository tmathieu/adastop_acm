
bench_fortran_guix ()
{
    # Compile
    guix time-machine --channels=../../../channels.scm -- shell gfortran-toolchain python -- gfortran Fortran/nbody.ifc-6.f90 -O3 -o /tmp/nbody.ifc-4.ifc_run

    # Run
    touch /tmp/energy_fortran_guix_$1.txt
    for f in $(seq 6); do
        result=`guix time-machine --channels=../../../channels.scm -- shell gfortran-toolchain python -- python3  utils/get_joules.py /tmp/nbody.ifc-4.ifc_run 50000000`
        sleep 3
        echo $result >> /tmp/energy_fortran_guix_$1.txt
    done
    
}


