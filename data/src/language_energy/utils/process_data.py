#!/usr/bin/env -S guix time-machine --channels=../../../channels.scm -- shell --pure python python-pandas python-tqdm -- python3

import pandas as pd
import glob
import sys
import numpy as np

idx = sys.argv[1]

df = pd.DataFrame()
for f in glob.glob("/tmp/energy_*_"+idx+".txt"):
    name = f.split("_")[1]
    with open(f, "r") as file:
        column = [float(num) for num in np.array(file.read().split('\n')) if len(num)>0]
    df[name]=column

df.to_csv("energy_"+idx+".csv")

