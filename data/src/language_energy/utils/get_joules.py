import subprocess
import sys
import re
import logging 

output = subprocess.run([ "perf", "stat", "-r", "1", "-e", "power/energy-pkg/"]+sys.argv[1:], capture_output=True)
logging.warning("output from perf is "+output.stderr.decode())

assert output.returncode == 0



res = re.findall("(\w+),(\w+)\s*Joules\s*", output.stderr.decode())

energy = float(".".join(res[0]))

print(energy)
# print(energy/(end-beg)) for Watt
